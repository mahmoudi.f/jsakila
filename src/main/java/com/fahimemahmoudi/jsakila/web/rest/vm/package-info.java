/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fahimemahmoudi.jsakila.web.rest.vm;
