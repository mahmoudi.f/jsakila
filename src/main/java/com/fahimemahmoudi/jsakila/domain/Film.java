package com.fahimemahmoudi.jsakila.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Film.
 */
@Entity
@Table(name = "film")
public class Film implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id @Column(name = "film_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "title", length = 255, nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "release_year")
    private Instant releaseYear;

    @NotNull
    @Column(name = "language_id", nullable = false)
    private Integer languageId;

    @NotNull
    @Column(name = "original_language_id", nullable = false)
    private Integer originalLanguageId;

    @NotNull
    @Column(name = "rental_duration", nullable = false)
    private Integer rentalDuration;

    @NotNull
    @Column(name = "rental_rate", nullable = false)
    private Double rentalRate;

    @Column(name = "length")
    private Integer length;

    @NotNull
    @Column(name = "replacement_cost", nullable = false)
    private Double replacementCost;

    @Column(name = "rating")
    private String rating;

    @Column(name = "special_features")
    private String specialFeatures;

    @NotNull
    @Column(name = "last_update", nullable = false)
    private Instant lastUpdate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Film title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Film description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getReleaseYear() {
        return releaseYear;
    }

    public Film releaseYear(Instant releaseYear) {
        this.releaseYear = releaseYear;
        return this;
    }

    public void setReleaseYear(Instant releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public Film languageId(Integer languageId) {
        this.languageId = languageId;
        return this;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getOriginalLanguageId() {
        return originalLanguageId;
    }

    public Film originalLanguageId(Integer originalLanguageId) {
        this.originalLanguageId = originalLanguageId;
        return this;
    }

    public void setOriginalLanguageId(Integer originalLanguageId) {
        this.originalLanguageId = originalLanguageId;
    }

    public Integer getRentalDuration() {
        return rentalDuration;
    }

    public Film rentalDuration(Integer rentalDuration) {
        this.rentalDuration = rentalDuration;
        return this;
    }

    public void setRentalDuration(Integer rentalDuration) {
        this.rentalDuration = rentalDuration;
    }

    public Double getRentalRate() {
        return rentalRate;
    }

    public Film rentalRate(Double rentalRate) {
        this.rentalRate = rentalRate;
        return this;
    }

    public void setRentalRate(Double rentalRate) {
        this.rentalRate = rentalRate;
    }

    public Integer getLength() {
        return length;
    }

    public Film length(Integer length) {
        this.length = length;
        return this;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Double getReplacementCost() {
        return replacementCost;
    }

    public Film replacementCost(Double replacementCost) {
        this.replacementCost = replacementCost;
        return this;
    }

    public void setReplacementCost(Double replacementCost) {
        this.replacementCost = replacementCost;
    }

    public String getRating() {
        return rating;
    }

    public Film rating(String rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSpecialFeatures() {
        return specialFeatures;
    }

    public Film specialFeatures(String specialFeatures) {
        this.specialFeatures = specialFeatures;
        return this;
    }

    public void setSpecialFeatures(String specialFeatures) {
        this.specialFeatures = specialFeatures;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public Film lastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Film)) {
            return false;
        }
        return id != null && id.equals(((Film) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Film{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", releaseYear='" + getReleaseYear() + "'" +
            ", languageId=" + getLanguageId() +
            ", originalLanguageId=" + getOriginalLanguageId() +
            ", rentalDuration=" + getRentalDuration() +
            ", rentalRate=" + getRentalRate() +
            ", length=" + getLength() +
            ", replacementCost=" + getReplacementCost() +
            ", rating='" + getRating() + "'" +
            ", specialFeatures='" + getSpecialFeatures() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
