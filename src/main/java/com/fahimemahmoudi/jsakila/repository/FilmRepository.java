package com.fahimemahmoudi.jsakila.repository;
import com.fahimemahmoudi.jsakila.domain.Film;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Film entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FilmRepository extends JpaRepository<Film, Long> {

}
