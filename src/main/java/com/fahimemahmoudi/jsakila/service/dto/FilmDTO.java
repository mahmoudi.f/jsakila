package com.fahimemahmoudi.jsakila.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.fahimemahmoudi.jsakila.domain.Film} entity.
 */
public class FilmDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 255)
    private String title;

    private String description;

    private Instant releaseYear;

    @NotNull
    private Integer languageId;

    @NotNull
    private Integer originalLanguageId;

    @NotNull
    private Integer rentalDuration;

    @NotNull
    private Double rentalRate;

    private Integer length;

    @NotNull
    private Double replacementCost;

    private String rating;

    private String specialFeatures;

    @NotNull
    private Instant lastUpdate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Instant getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Instant releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getOriginalLanguageId() {
        return originalLanguageId;
    }

    public void setOriginalLanguageId(Integer originalLanguageId) {
        this.originalLanguageId = originalLanguageId;
    }

    public Integer getRentalDuration() {
        return rentalDuration;
    }

    public void setRentalDuration(Integer rentalDuration) {
        this.rentalDuration = rentalDuration;
    }

    public Double getRentalRate() {
        return rentalRate;
    }

    public void setRentalRate(Double rentalRate) {
        this.rentalRate = rentalRate;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Double getReplacementCost() {
        return replacementCost;
    }

    public void setReplacementCost(Double replacementCost) {
        this.replacementCost = replacementCost;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getSpecialFeatures() {
        return specialFeatures;
    }

    public void setSpecialFeatures(String specialFeatures) {
        this.specialFeatures = specialFeatures;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FilmDTO filmDTO = (FilmDTO) o;
        if (filmDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), filmDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FilmDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", releaseYear='" + getReleaseYear() + "'" +
            ", languageId=" + getLanguageId() +
            ", originalLanguageId=" + getOriginalLanguageId() +
            ", rentalDuration=" + getRentalDuration() +
            ", rentalRate=" + getRentalRate() +
            ", length=" + getLength() +
            ", replacementCost=" + getReplacementCost() +
            ", rating='" + getRating() + "'" +
            ", specialFeatures='" + getSpecialFeatures() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
