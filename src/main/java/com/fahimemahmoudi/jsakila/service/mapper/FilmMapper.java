package com.fahimemahmoudi.jsakila.service.mapper;

import com.fahimemahmoudi.jsakila.domain.*;
import com.fahimemahmoudi.jsakila.service.dto.FilmDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Film} and its DTO {@link FilmDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FilmMapper extends EntityMapper<FilmDTO, Film> {



    default Film fromId(Long id) {
        if (id == null) {
            return null;
        }
        Film film = new Film();
        film.setId(id);
        return film;
    }
}
