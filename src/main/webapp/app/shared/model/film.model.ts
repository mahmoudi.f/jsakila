import { Moment } from 'moment';

export interface IFilm {
  id?: number;
  title?: string;
  description?: string;
  releaseYear?: Moment;
  languageId?: number;
  originalLanguageId?: number;
  rentalDuration?: number;
  rentalRate?: number;
  length?: number;
  replacementCost?: number;
  rating?: string;
  specialFeatures?: string;
  lastUpdate?: Moment;
}

export const defaultValue: Readonly<IFilm> = {};
