import { Moment } from 'moment';

export interface IActor {
  id?: number;
  firstName?: string;
  lastName?: string;
  lastUpdate?: Moment;
}

export const defaultValue: Readonly<IActor> = {};
