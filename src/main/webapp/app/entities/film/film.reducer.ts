import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IFilm, defaultValue } from 'app/shared/model/film.model';

export const ACTION_TYPES = {
  FETCH_FILM_LIST: 'film/FETCH_FILM_LIST',
  FETCH_FILM: 'film/FETCH_FILM',
  CREATE_FILM: 'film/CREATE_FILM',
  UPDATE_FILM: 'film/UPDATE_FILM',
  DELETE_FILM: 'film/DELETE_FILM',
  RESET: 'film/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IFilm>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type FilmState = Readonly<typeof initialState>;

// Reducer

export default (state: FilmState = initialState, action): FilmState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_FILM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_FILM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_FILM):
    case REQUEST(ACTION_TYPES.UPDATE_FILM):
    case REQUEST(ACTION_TYPES.DELETE_FILM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_FILM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_FILM):
    case FAILURE(ACTION_TYPES.CREATE_FILM):
    case FAILURE(ACTION_TYPES.UPDATE_FILM):
    case FAILURE(ACTION_TYPES.DELETE_FILM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_FILM_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_FILM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_FILM):
    case SUCCESS(ACTION_TYPES.UPDATE_FILM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_FILM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/films';

// Actions

export const getEntities: ICrudGetAllAction<IFilm> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_FILM_LIST,
    payload: axios.get<IFilm>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IFilm> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_FILM,
    payload: axios.get<IFilm>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IFilm> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_FILM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IFilm> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_FILM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IFilm> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_FILM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
