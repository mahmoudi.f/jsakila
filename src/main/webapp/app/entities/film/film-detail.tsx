import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './film.reducer';
import { IFilm } from 'app/shared/model/film.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFilmDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class FilmDetail extends React.Component<IFilmDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { filmEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Film [<b>{filmEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="title">Title</span>
            </dt>
            <dd>{filmEntity.title}</dd>
            <dt>
              <span id="description">Description</span>
            </dt>
            <dd>{filmEntity.description}</dd>
            <dt>
              <span id="releaseYear">Release Year</span>
            </dt>
            <dd>
              <TextFormat value={filmEntity.releaseYear} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="languageId">Language Id</span>
            </dt>
            <dd>{filmEntity.languageId}</dd>
            <dt>
              <span id="originalLanguageId">Original Language Id</span>
            </dt>
            <dd>{filmEntity.originalLanguageId}</dd>
            <dt>
              <span id="rentalDuration">Rental Duration</span>
            </dt>
            <dd>{filmEntity.rentalDuration}</dd>
            <dt>
              <span id="rentalRate">Rental Rate</span>
            </dt>
            <dd>{filmEntity.rentalRate}</dd>
            <dt>
              <span id="length">Length</span>
            </dt>
            <dd>{filmEntity.length}</dd>
            <dt>
              <span id="replacementCost">Replacement Cost</span>
            </dt>
            <dd>{filmEntity.replacementCost}</dd>
            <dt>
              <span id="rating">Rating</span>
            </dt>
            <dd>{filmEntity.rating}</dd>
            <dt>
              <span id="specialFeatures">Special Features</span>
            </dt>
            <dd>{filmEntity.specialFeatures}</dd>
            <dt>
              <span id="lastUpdate">Last Update</span>
            </dt>
            <dd>
              <TextFormat value={filmEntity.lastUpdate} type="date" format={APP_DATE_FORMAT} />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/film" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/film/${filmEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ film }: IRootState) => ({
  filmEntity: film.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilmDetail);
