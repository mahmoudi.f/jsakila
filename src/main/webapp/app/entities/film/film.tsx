import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './film.reducer';
import { IFilm } from 'app/shared/model/film.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IFilmProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IFilmState = IPaginationBaseState;

export class Film extends React.Component<IFilmProps, IFilmState> {
  state: IFilmState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { filmList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="film-heading">
          Films
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create a new Film
          </Link>
        </h2>
        <div className="table-responsive">
          {filmList && filmList.length > 0 ? (
            <Table responsive aria-describedby="film-heading">
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    ID <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('title')}>
                    Title <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('description')}>
                    Description <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('releaseYear')}>
                    Release Year <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('languageId')}>
                    Language Id <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('originalLanguageId')}>
                    Original Language Id <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('rentalDuration')}>
                    Rental Duration <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('rentalRate')}>
                    Rental Rate <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('length')}>
                    Length <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('replacementCost')}>
                    Replacement Cost <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('rating')}>
                    Rating <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('specialFeatures')}>
                    Special Features <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('lastUpdate')}>
                    Last Update <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {filmList.map((film, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${film.id}`} color="link" size="sm">
                        {film.id}
                      </Button>
                    </td>
                    <td>{film.title}</td>
                    <td>{film.description}</td>
                    <td>
                      <TextFormat type="date" value={film.releaseYear} format={APP_DATE_FORMAT} />
                    </td>
                    <td>{film.languageId}</td>
                    <td>{film.originalLanguageId}</td>
                    <td>{film.rentalDuration}</td>
                    <td>{film.rentalRate}</td>
                    <td>{film.length}</td>
                    <td>{film.replacementCost}</td>
                    <td>{film.rating}</td>
                    <td>{film.specialFeatures}</td>
                    <td>
                      <TextFormat type="date" value={film.lastUpdate} format={APP_DATE_FORMAT} />
                    </td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${film.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${film.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${film.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Films found</div>
          )}
        </div>
        <div className={filmList && filmList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={this.state.activePage} total={totalItems} itemsPerPage={this.state.itemsPerPage} />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={this.state.activePage}
              onSelect={this.handlePagination}
              maxButtons={5}
              itemsPerPage={this.state.itemsPerPage}
              totalItems={this.props.totalItems}
            />
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ film }: IRootState) => ({
  filmList: film.entities,
  totalItems: film.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Film);
