import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './film.reducer';
import { IFilm } from 'app/shared/model/film.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IFilmUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IFilmUpdateState {
  isNew: boolean;
}

export class FilmUpdate extends React.Component<IFilmUpdateProps, IFilmUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.releaseYear = convertDateTimeToServer(values.releaseYear);
    values.lastUpdate = convertDateTimeToServer(values.lastUpdate);

    if (errors.length === 0) {
      const { filmEntity } = this.props;
      const entity = {
        ...filmEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/film');
  };

  render() {
    const { filmEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="sakilaApp.film.home.createOrEditLabel">Create or edit a Film</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : filmEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="film-id">ID</Label>
                    <AvInput id="film-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="titleLabel" for="film-title">
                    Title
                  </Label>
                  <AvField
                    id="film-title"
                    type="text"
                    name="title"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 255, errorMessage: 'This field cannot be longer than 255 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="film-description">
                    Description
                  </Label>
                  <AvField id="film-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="releaseYearLabel" for="film-releaseYear">
                    Release Year
                  </Label>
                  <AvInput
                    id="film-releaseYear"
                    type="datetime-local"
                    className="form-control"
                    name="releaseYear"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.filmEntity.releaseYear)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="languageIdLabel" for="film-languageId">
                    Language Id
                  </Label>
                  <AvField
                    id="film-languageId"
                    type="string"
                    className="form-control"
                    name="languageId"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="originalLanguageIdLabel" for="film-originalLanguageId">
                    Original Language Id
                  </Label>
                  <AvField
                    id="film-originalLanguageId"
                    type="string"
                    className="form-control"
                    name="originalLanguageId"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="rentalDurationLabel" for="film-rentalDuration">
                    Rental Duration
                  </Label>
                  <AvField
                    id="film-rentalDuration"
                    type="string"
                    className="form-control"
                    name="rentalDuration"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="rentalRateLabel" for="film-rentalRate">
                    Rental Rate
                  </Label>
                  <AvField
                    id="film-rentalRate"
                    type="string"
                    className="form-control"
                    name="rentalRate"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="lengthLabel" for="film-length">
                    Length
                  </Label>
                  <AvField id="film-length" type="string" className="form-control" name="length" />
                </AvGroup>
                <AvGroup>
                  <Label id="replacementCostLabel" for="film-replacementCost">
                    Replacement Cost
                  </Label>
                  <AvField
                    id="film-replacementCost"
                    type="string"
                    className="form-control"
                    name="replacementCost"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="ratingLabel" for="film-rating">
                    Rating
                  </Label>
                  <AvField id="film-rating" type="text" name="rating" />
                </AvGroup>
                <AvGroup>
                  <Label id="specialFeaturesLabel" for="film-specialFeatures">
                    Special Features
                  </Label>
                  <AvField id="film-specialFeatures" type="text" name="specialFeatures" />
                </AvGroup>
                <AvGroup>
                  <Label id="lastUpdateLabel" for="film-lastUpdate">
                    Last Update
                  </Label>
                  <AvInput
                    id="film-lastUpdate"
                    type="datetime-local"
                    className="form-control"
                    name="lastUpdate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.filmEntity.lastUpdate)}
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/film" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  filmEntity: storeState.film.entity,
  loading: storeState.film.loading,
  updating: storeState.film.updating,
  updateSuccess: storeState.film.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilmUpdate);
