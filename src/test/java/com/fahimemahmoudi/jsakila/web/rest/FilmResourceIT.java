package com.fahimemahmoudi.jsakila.web.rest;

import com.fahimemahmoudi.jsakila.SakilaApp;
import com.fahimemahmoudi.jsakila.domain.Film;
import com.fahimemahmoudi.jsakila.repository.FilmRepository;
import com.fahimemahmoudi.jsakila.service.FilmService;
import com.fahimemahmoudi.jsakila.service.dto.FilmDTO;
import com.fahimemahmoudi.jsakila.service.mapper.FilmMapper;
import com.fahimemahmoudi.jsakila.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.fahimemahmoudi.jsakila.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FilmResource} REST controller.
 */
@SpringBootTest(classes = SakilaApp.class)
public class FilmResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_RELEASE_YEAR = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_RELEASE_YEAR = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_RELEASE_YEAR = Instant.ofEpochMilli(-1L);

    private static final Integer DEFAULT_LANGUAGE_ID = 1;
    private static final Integer UPDATED_LANGUAGE_ID = 2;
    private static final Integer SMALLER_LANGUAGE_ID = 1 - 1;

    private static final Integer DEFAULT_ORIGINAL_LANGUAGE_ID = 1;
    private static final Integer UPDATED_ORIGINAL_LANGUAGE_ID = 2;
    private static final Integer SMALLER_ORIGINAL_LANGUAGE_ID = 1 - 1;

    private static final Integer DEFAULT_RENTAL_DURATION = 1;
    private static final Integer UPDATED_RENTAL_DURATION = 2;
    private static final Integer SMALLER_RENTAL_DURATION = 1 - 1;

    private static final Double DEFAULT_RENTAL_RATE = 1D;
    private static final Double UPDATED_RENTAL_RATE = 2D;
    private static final Double SMALLER_RENTAL_RATE = 1D - 1D;

    private static final Integer DEFAULT_LENGTH = 1;
    private static final Integer UPDATED_LENGTH = 2;
    private static final Integer SMALLER_LENGTH = 1 - 1;

    private static final Double DEFAULT_REPLACEMENT_COST = 1D;
    private static final Double UPDATED_REPLACEMENT_COST = 2D;
    private static final Double SMALLER_REPLACEMENT_COST = 1D - 1D;

    private static final String DEFAULT_RATING = "AAAAAAAAAA";
    private static final String UPDATED_RATING = "BBBBBBBBBB";

    private static final String DEFAULT_SPECIAL_FEATURES = "AAAAAAAAAA";
    private static final String UPDATED_SPECIAL_FEATURES = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_UPDATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_UPDATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_LAST_UPDATE = Instant.ofEpochMilli(-1L);

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private FilmMapper filmMapper;

    @Autowired
    private FilmService filmService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFilmMockMvc;

    private Film film;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FilmResource filmResource = new FilmResource(filmService);
        this.restFilmMockMvc = MockMvcBuilders.standaloneSetup(filmResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Film createEntity(EntityManager em) {
        Film film = new Film()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .releaseYear(DEFAULT_RELEASE_YEAR)
            .languageId(DEFAULT_LANGUAGE_ID)
            .originalLanguageId(DEFAULT_ORIGINAL_LANGUAGE_ID)
            .rentalDuration(DEFAULT_RENTAL_DURATION)
            .rentalRate(DEFAULT_RENTAL_RATE)
            .length(DEFAULT_LENGTH)
            .replacementCost(DEFAULT_REPLACEMENT_COST)
            .rating(DEFAULT_RATING)
            .specialFeatures(DEFAULT_SPECIAL_FEATURES)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return film;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Film createUpdatedEntity(EntityManager em) {
        Film film = new Film()
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .releaseYear(UPDATED_RELEASE_YEAR)
            .languageId(UPDATED_LANGUAGE_ID)
            .originalLanguageId(UPDATED_ORIGINAL_LANGUAGE_ID)
            .rentalDuration(UPDATED_RENTAL_DURATION)
            .rentalRate(UPDATED_RENTAL_RATE)
            .length(UPDATED_LENGTH)
            .replacementCost(UPDATED_REPLACEMENT_COST)
            .rating(UPDATED_RATING)
            .specialFeatures(UPDATED_SPECIAL_FEATURES)
            .lastUpdate(UPDATED_LAST_UPDATE);
        return film;
    }

    @BeforeEach
    public void initTest() {
        film = createEntity(em);
    }

    @Test
    @Transactional
    public void createFilm() throws Exception {
        int databaseSizeBeforeCreate = filmRepository.findAll().size();

        // Create the Film
        FilmDTO filmDTO = filmMapper.toDto(film);
        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isCreated());

        // Validate the Film in the database
        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeCreate + 1);
        Film testFilm = filmList.get(filmList.size() - 1);
        assertThat(testFilm.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testFilm.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFilm.getReleaseYear()).isEqualTo(DEFAULT_RELEASE_YEAR);
        assertThat(testFilm.getLanguageId()).isEqualTo(DEFAULT_LANGUAGE_ID);
        assertThat(testFilm.getOriginalLanguageId()).isEqualTo(DEFAULT_ORIGINAL_LANGUAGE_ID);
        assertThat(testFilm.getRentalDuration()).isEqualTo(DEFAULT_RENTAL_DURATION);
        assertThat(testFilm.getRentalRate()).isEqualTo(DEFAULT_RENTAL_RATE);
        assertThat(testFilm.getLength()).isEqualTo(DEFAULT_LENGTH);
        assertThat(testFilm.getReplacementCost()).isEqualTo(DEFAULT_REPLACEMENT_COST);
        assertThat(testFilm.getRating()).isEqualTo(DEFAULT_RATING);
        assertThat(testFilm.getSpecialFeatures()).isEqualTo(DEFAULT_SPECIAL_FEATURES);
        assertThat(testFilm.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void createFilmWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = filmRepository.findAll().size();

        // Create the Film with an existing ID
        film.setId(1L);
        FilmDTO filmDTO = filmMapper.toDto(film);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Film in the database
        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = filmRepository.findAll().size();
        // set the field null
        film.setTitle(null);

        // Create the Film, which fails.
        FilmDTO filmDTO = filmMapper.toDto(film);

        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLanguageIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = filmRepository.findAll().size();
        // set the field null
        film.setLanguageId(null);

        // Create the Film, which fails.
        FilmDTO filmDTO = filmMapper.toDto(film);

        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkOriginalLanguageIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = filmRepository.findAll().size();
        // set the field null
        film.setOriginalLanguageId(null);

        // Create the Film, which fails.
        FilmDTO filmDTO = filmMapper.toDto(film);

        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRentalDurationIsRequired() throws Exception {
        int databaseSizeBeforeTest = filmRepository.findAll().size();
        // set the field null
        film.setRentalDuration(null);

        // Create the Film, which fails.
        FilmDTO filmDTO = filmMapper.toDto(film);

        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRentalRateIsRequired() throws Exception {
        int databaseSizeBeforeTest = filmRepository.findAll().size();
        // set the field null
        film.setRentalRate(null);

        // Create the Film, which fails.
        FilmDTO filmDTO = filmMapper.toDto(film);

        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReplacementCostIsRequired() throws Exception {
        int databaseSizeBeforeTest = filmRepository.findAll().size();
        // set the field null
        film.setReplacementCost(null);

        // Create the Film, which fails.
        FilmDTO filmDTO = filmMapper.toDto(film);

        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = filmRepository.findAll().size();
        // set the field null
        film.setLastUpdate(null);

        // Create the Film, which fails.
        FilmDTO filmDTO = filmMapper.toDto(film);

        restFilmMockMvc.perform(post("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFilms() throws Exception {
        // Initialize the database
        filmRepository.saveAndFlush(film);

        // Get all the filmList
        restFilmMockMvc.perform(get("/api/films?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(film.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].releaseYear").value(hasItem(DEFAULT_RELEASE_YEAR.toString())))
            .andExpect(jsonPath("$.[*].languageId").value(hasItem(DEFAULT_LANGUAGE_ID)))
            .andExpect(jsonPath("$.[*].originalLanguageId").value(hasItem(DEFAULT_ORIGINAL_LANGUAGE_ID)))
            .andExpect(jsonPath("$.[*].rentalDuration").value(hasItem(DEFAULT_RENTAL_DURATION)))
            .andExpect(jsonPath("$.[*].rentalRate").value(hasItem(DEFAULT_RENTAL_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH)))
            .andExpect(jsonPath("$.[*].replacementCost").value(hasItem(DEFAULT_REPLACEMENT_COST.doubleValue())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING.toString())))
            .andExpect(jsonPath("$.[*].specialFeatures").value(hasItem(DEFAULT_SPECIAL_FEATURES.toString())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.toString())));
    }
    
    @Test
    @Transactional
    public void getFilm() throws Exception {
        // Initialize the database
        filmRepository.saveAndFlush(film);

        // Get the film
        restFilmMockMvc.perform(get("/api/films/{id}", film.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(film.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.releaseYear").value(DEFAULT_RELEASE_YEAR.toString()))
            .andExpect(jsonPath("$.languageId").value(DEFAULT_LANGUAGE_ID))
            .andExpect(jsonPath("$.originalLanguageId").value(DEFAULT_ORIGINAL_LANGUAGE_ID))
            .andExpect(jsonPath("$.rentalDuration").value(DEFAULT_RENTAL_DURATION))
            .andExpect(jsonPath("$.rentalRate").value(DEFAULT_RENTAL_RATE.doubleValue()))
            .andExpect(jsonPath("$.length").value(DEFAULT_LENGTH))
            .andExpect(jsonPath("$.replacementCost").value(DEFAULT_REPLACEMENT_COST.doubleValue()))
            .andExpect(jsonPath("$.rating").value(DEFAULT_RATING.toString()))
            .andExpect(jsonPath("$.specialFeatures").value(DEFAULT_SPECIAL_FEATURES.toString()))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFilm() throws Exception {
        // Get the film
        restFilmMockMvc.perform(get("/api/films/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFilm() throws Exception {
        // Initialize the database
        filmRepository.saveAndFlush(film);

        int databaseSizeBeforeUpdate = filmRepository.findAll().size();

        // Update the film
        Film updatedFilm = filmRepository.findById(film.getId()).get();
        // Disconnect from session so that the updates on updatedFilm are not directly saved in db
        em.detach(updatedFilm);
        updatedFilm
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .releaseYear(UPDATED_RELEASE_YEAR)
            .languageId(UPDATED_LANGUAGE_ID)
            .originalLanguageId(UPDATED_ORIGINAL_LANGUAGE_ID)
            .rentalDuration(UPDATED_RENTAL_DURATION)
            .rentalRate(UPDATED_RENTAL_RATE)
            .length(UPDATED_LENGTH)
            .replacementCost(UPDATED_REPLACEMENT_COST)
            .rating(UPDATED_RATING)
            .specialFeatures(UPDATED_SPECIAL_FEATURES)
            .lastUpdate(UPDATED_LAST_UPDATE);
        FilmDTO filmDTO = filmMapper.toDto(updatedFilm);

        restFilmMockMvc.perform(put("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isOk());

        // Validate the Film in the database
        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeUpdate);
        Film testFilm = filmList.get(filmList.size() - 1);
        assertThat(testFilm.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testFilm.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFilm.getReleaseYear()).isEqualTo(UPDATED_RELEASE_YEAR);
        assertThat(testFilm.getLanguageId()).isEqualTo(UPDATED_LANGUAGE_ID);
        assertThat(testFilm.getOriginalLanguageId()).isEqualTo(UPDATED_ORIGINAL_LANGUAGE_ID);
        assertThat(testFilm.getRentalDuration()).isEqualTo(UPDATED_RENTAL_DURATION);
        assertThat(testFilm.getRentalRate()).isEqualTo(UPDATED_RENTAL_RATE);
        assertThat(testFilm.getLength()).isEqualTo(UPDATED_LENGTH);
        assertThat(testFilm.getReplacementCost()).isEqualTo(UPDATED_REPLACEMENT_COST);
        assertThat(testFilm.getRating()).isEqualTo(UPDATED_RATING);
        assertThat(testFilm.getSpecialFeatures()).isEqualTo(UPDATED_SPECIAL_FEATURES);
        assertThat(testFilm.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    @Transactional
    public void updateNonExistingFilm() throws Exception {
        int databaseSizeBeforeUpdate = filmRepository.findAll().size();

        // Create the Film
        FilmDTO filmDTO = filmMapper.toDto(film);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFilmMockMvc.perform(put("/api/films")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(filmDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Film in the database
        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFilm() throws Exception {
        // Initialize the database
        filmRepository.saveAndFlush(film);

        int databaseSizeBeforeDelete = filmRepository.findAll().size();

        // Delete the film
        restFilmMockMvc.perform(delete("/api/films/{id}", film.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Film> filmList = filmRepository.findAll();
        assertThat(filmList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Film.class);
        Film film1 = new Film();
        film1.setId(1L);
        Film film2 = new Film();
        film2.setId(film1.getId());
        assertThat(film1).isEqualTo(film2);
        film2.setId(2L);
        assertThat(film1).isNotEqualTo(film2);
        film1.setId(null);
        assertThat(film1).isNotEqualTo(film2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FilmDTO.class);
        FilmDTO filmDTO1 = new FilmDTO();
        filmDTO1.setId(1L);
        FilmDTO filmDTO2 = new FilmDTO();
        assertThat(filmDTO1).isNotEqualTo(filmDTO2);
        filmDTO2.setId(filmDTO1.getId());
        assertThat(filmDTO1).isEqualTo(filmDTO2);
        filmDTO2.setId(2L);
        assertThat(filmDTO1).isNotEqualTo(filmDTO2);
        filmDTO1.setId(null);
        assertThat(filmDTO1).isNotEqualTo(filmDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(filmMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(filmMapper.fromId(null)).isNull();
    }
}
